import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from 'src/app/shared/services/authentication-service.service';
import {NavController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class IsNotSignedInGuard implements CanActivate {

    constructor(private authService: AuthenticationService,
                private navCtrl: NavController) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if ( this.authService.isLoggedIn ) {
            this.navCtrl.navigateRoot('/home');
        }
        return !this.authService.isLoggedIn;
    }
}
