import { Injectable } from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  constructor(
      public toastController: ToastController) {
  }

  async videoStartRendering() {
    const toast = await this.toastController.create({
      header: 'Your video has now started to render !',
      message: 'You can see the progress in the corresponding tab here',
      duration: 2500
    });
    toast.present();
  }

  async fillAllInput() {
    const toast = await this.toastController.create({
      header: 'You didn\'t fill all the inputs',
      message: 'Please try again',
      duration: 2500
    });
    toast.present();
  }
  async videoDeleted() {
    const toast = await this.toastController.create({
      header: 'Your video has been deleted with success',
      duration: 2500
    });
    toast.present();
  }


}
