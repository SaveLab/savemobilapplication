import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FreeTrialREPage } from './free-trial-re.page';

describe('FreeTrialREPage', () => {
  let component: FreeTrialREPage;
  let fixture: ComponentFixture<FreeTrialREPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeTrialREPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FreeTrialREPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
