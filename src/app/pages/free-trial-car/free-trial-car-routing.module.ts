import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FreeTrialCarPage } from './free-trial-car.page';

const routes: Routes = [
  {
    path: '',
    component: FreeTrialCarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FreeTrialCarPageRoutingModule {}
