import { Component, OnInit } from '@angular/core';
import {ModalController} from "@ionic/angular";
import {AuthenticationService} from "../../services/authentication-service.service";
import {DataService} from "../../services/data.service";
import {environment} from "../../../../environments/environment";
declare var Stripe;

@Component({
  selector: 'app-modal-credit-card',
  templateUrl: './modal-credit-card.component.html',
  styleUrls: ['./modal-credit-card.component.scss'],
})
export class ModalCreditCardComponent implements OnInit {


  number: string;
  stripe = Stripe(environment.stripeKey);
  card: any;

  constructor(
      public modalController: ModalController,
      public dataService: DataService,
      public authService: AuthenticationService) {
  }

  ngOnInit() {
    this.setupStripe();
  }

  setupStripe() {
    const elements = this.stripe.elements();
    const style = {
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    this.card = elements.create('card', { style });
    this.card.mount('#card-element');

    this.card.addEventListener('change', event => {
      const displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });
  }

  send() {
    this.stripe.createToken(this.card).then(result => {
      if (result.error) {
        const errorElement = document.getElementById('card-errors');
        errorElement.textContent = result.error.message;
      } else {
        this.modalController.dismiss(result);
      }
    });
  }

  close() {
    this.modalController.dismiss();
  }

}
