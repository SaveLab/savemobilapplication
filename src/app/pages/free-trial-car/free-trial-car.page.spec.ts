import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FreeTrialCarPage } from './free-trial-car.page';

describe('FreeTrialCarPage', () => {
  let component: FreeTrialCarPage;
  let fixture: ComponentFixture<FreeTrialCarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeTrialCarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FreeTrialCarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
