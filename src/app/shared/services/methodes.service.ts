import { Injectable } from '@angular/core';
import {AuthenticationService} from "./authentication-service.service";
import {NavController, ToastController} from "@ionic/angular";

@Injectable({
  providedIn: 'root'
})
export class MethodesService {

  constructor(private authService: AuthenticationService,
              private navCtrl: NavController,
              public toastController: ToastController
  ) {  }

  goToFreeTrial(page){
    if (this.authService.isLoggedIn){
      this.navCtrl.navigateRoot('/' + page);
    }else{
      this.needBeLoggedIn();
      this.navCtrl.navigateRoot('sign-in');
    }
  }

  async needBeLoggedIn() {
    const toast = await this.toastController.create({
      header: 'You need to be logged-in to access this fonctionality',
      message: 'Sign-in before then try again.',
      duration: 3000
    });
    toast.present();
  }

  goTo(page){
    this.navCtrl.navigateRoot('/' + page);
  }



}
