const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require("cors")({origin: true});
const stripe = require('stripe')('sk_test_51H4ioCE8ESXHasdjmSkzI7RuFWxMbAPsdPUg1Of6GLnrRqJoPRiL70UYrGCmq3A45I6A4fAGbZH247UrRUoQom2m004rauxljn');

admin.initializeApp();

const db = admin.firestore();

exports.countVideoRendered = functions.firestore.document('Users/{userID}/renderedVideo/{field}/data/{videoID}').onCreate((snap, context) => {

    const userID = context.params.userID;
    console.log('le user id est: ' + userID);

    db.collection('Users').doc(userID).get().then(user => {
        const numberOfVideoAlreadyRendered = user.data().videoRendered;
        console.log('number of video already rendered: ' + numberOfVideoAlreadyRendered);

        return db.collection('Users').doc(userID).update({
            videoRendered: numberOfVideoAlreadyRendered + 1
        }).then()
            .catch(err => {
                console.log(err);
                return false
            });

    }).catch(err => {
        return console.log(err);
    });


});

exports.countVideoRenderedDeleted = functions.firestore.document('Users/{userID}/renderedVideo/{field}/data/{videoID}').onDelete((snap, context) => {

    const userID = context.params.userID;
    console.log('le user id est: ' + userID);

    db.collection('Users').doc(userID).get().then(user => {
        const numberOfVideoAlreadyRendered = user.data().videoRendered;
        console.log('number of video already rendered: ' + numberOfVideoAlreadyRendered);

        return db.collection('Users').doc(userID).update({
            videoRendered: numberOfVideoAlreadyRendered - 1
        }).then()
            .catch(err => {
                console.log(err);
                return false
            });

    }).catch(err => {
        return console.log(err);
    });


});
