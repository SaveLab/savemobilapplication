import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaveCVsPageRoutingModule } from './save-cvs-routing.module';

import { SaveCVsPage } from './save-cvs.page';
import {ComponentModule} from "../../shared/components/component.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SaveCVsPageRoutingModule,
    ComponentModule
  ],
  declarations: [SaveCVsPage]
})
export class SaveCVsPageModule {}
