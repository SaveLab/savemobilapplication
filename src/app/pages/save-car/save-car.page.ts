import {Component, OnInit} from '@angular/core';
import {MethodesService} from "../../shared/services/methodes.service";
import {ModalService} from "../../shared/services/modal.service";
import {AuthenticationService} from "../../shared/services/authentication-service.service";

@Component({
    selector: 'app-save-car',
    templateUrl: './save-car.page.html',
    styleUrls: ['./save-car.page.scss'],
})
export class SaveCarPage implements OnInit {
    selectedValues: string;

    constructor(
        public methodesService: MethodesService,
        public modalService: ModalService,
        public authService: AuthenticationService
    ) {
        this.selectedValues = 'howitworks'


    }

    ngOnInit() {
    }

    segmentChanged(ev: any) {
        this.selectedValues = ev.target.value;

    }

    goToFreeTrial(){
        if(this.authService.userData.freeCar){
            this.seePricing();
            this.modalService.showFreeTrialAlreadyDoneModal('Save Car');
        }else {
            this.methodesService.goToFreeTrial('free-trial-car');
        }
    }

    seePricing() {
        this.selectedValues = 'pricing';
    }

}
