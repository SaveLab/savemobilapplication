import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {HeaderComponent} from "./header/header.component";
import {FooterComponent} from "./footer/footer.component";
import {PopOverMembershipComponent} from "./pop-over-membership/pop-over-membership.component";



@NgModule({
    imports: [

        CommonModule,
        FormsModule,
        IonicModule
    ],
    declarations: [
        HeaderComponent,
        FooterComponent,
        PopOverMembershipComponent

    ],
    providers: [],
    exports: [
        FooterComponent,
        HeaderComponent,
        PopOverMembershipComponent,
    ]
})

export class ComponentModule {
}
