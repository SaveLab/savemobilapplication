import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FreeTrialREPageRoutingModule } from './free-trial-re-routing.module';

import { FreeTrialREPage } from './free-trial-re.page';
import {ComponentModule} from "../../shared/components/component.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FreeTrialREPageRoutingModule,
        ComponentModule
    ],
  declarations: [FreeTrialREPage]
})
export class FreeTrialREPageModule {}
