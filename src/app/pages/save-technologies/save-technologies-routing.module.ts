import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SaveTechnologiesPage } from './save-technologies.page';

const routes: Routes = [
  {
    path: '',
    component: SaveTechnologiesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SaveTechnologiesPageRoutingModule {}
