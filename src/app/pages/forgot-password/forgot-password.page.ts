import {Component, OnInit} from '@angular/core';

import {Router} from '@angular/router';
import {AuthenticationService} from 'src/app/shared/services/authentication-service.service';
import {NavController, ToastController} from '@ionic/angular';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit{

  email: any;
  errorMessage: any;
  loading = false;

  constructor(
              private navCtrl: NavController,
              public router: Router,
              public toastController: ToastController,
              private authService: AuthenticationService) {}

  ngOnInit(): void {
  }

  resetPassword() {
    this.loading = true;
    this.authService.passwordRecover(this.email).then(() => {
      this.loading = false;
      this.resetLinkSent();
    }).catch(err => {
      this.loading = false;
      this.errorMessage = err.message;
      throw err;
    });
  }

  goTo(page: string) {
    this.navCtrl.navigateRoot('/' + page);
  }

  async resetLinkSent() {
    const toast = await this.toastController.create({
      header: 'An email has been sent to your email address',
      message: 'Click on the link to reset your password.',
      duration: 5000
    });
    toast.present();
  }
}
