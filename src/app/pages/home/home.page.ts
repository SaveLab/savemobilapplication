import {Component} from '@angular/core';
import {MethodesService} from "../../shared/services/methodes.service";
import {AuthenticationService} from "../../shared/services/authentication-service.service";
import {LanguagesService} from "../../shared/services/languages.service";

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    constructor(
        public methodesService: MethodesService,
        public authService: AuthenticationService,
        public languageService: LanguagesService,
    ){}


    gotoUpload(){
        window.open(        'https://upload-save-technologies.web.app/home', '_system')

    }



}
