import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {UploadHelpComponent} from './upload-help/upload-help.component';
import {FreeTrialsDoneComponent} from "./free-trials-done/free-trials-done.component";
import {ModalCreditCardComponent} from "./modal-credit-card/modal-credit-card.component";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule
    ],
    declarations: [
        UploadHelpComponent,
        FreeTrialsDoneComponent,
        ModalCreditCardComponent


    ],
    providers: [],
    exports: [
        UploadHelpComponent,
        FreeTrialsDoneComponent,
        ModalCreditCardComponent
    ]
})

export class ModalModule {
}
