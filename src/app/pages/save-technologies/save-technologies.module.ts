import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaveTechnologiesPageRoutingModule } from './save-technologies-routing.module';

import { SaveTechnologiesPage } from './save-technologies.page';
import {ComponentModule} from "../../shared/components/component.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SaveTechnologiesPageRoutingModule,
        ComponentModule
    ],
  declarations: [SaveTechnologiesPage]
})
export class SaveTechnologiesPageModule {}
