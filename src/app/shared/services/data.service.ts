import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AngularFirestore} from "@angular/fire/firestore";
import {Platform, ToastController} from "@ionic/angular";
import {AuthenticationService} from "./authentication-service.service";

@Injectable({
    providedIn: 'root'
})
export class DataService {




    videosRealEstate = [];
    videosCvs = [];
    videosCars = [];


    constructor(
        public router: Router,
        public aFS: AngularFirestore,
        public platform: Platform,
        public toastController: ToastController,
        public authService: AuthenticationService) {

        console.log('JE SUISSSSSSSSSS LAAAAAAAAA');
        if (this.authService.isLoggedIn) {

            this.loadUserData();
        }


    }

    goto(page: string) {
        console.log(page)

    }

    deleteVideoRendered(field: string, idVideo: string) {


        this.aFS.collection('Users')
            .doc(this.authService.userData.uid)
            .collection('renderedVideo')
            .doc(field)
            .collection('data')
            .doc(idVideo)
            .delete()
            .then(res => {
                console.log('succeded to delete the renderedVideo')
            })
            .catch(err => {
                console.log(err)
            })

    }

    loadUserData() {
        this.aFS.collection('Users')
            .doc(this.authService.userData.uid)
            .collection('renderedVideo')
            .doc('realEstate')
            .collection('data')
            .valueChanges()
            .subscribe(videos => {
                videos.forEach(video => {
                    this.videosRealEstate.push(video);
                });
            });
        this.aFS.collection('Users')
            .doc(this.authService.userData.uid)
            .collection('renderedVideo')
            .doc('cv')
            .collection('data')
            .valueChanges()
            .subscribe(videos => {
                videos.forEach(video => {
                    this.videosCvs.push(video);
                });
            });
        this.aFS.collection('Users')
            .doc(this.authService.userData.uid)
            .collection('renderedVideo')
            .doc('car')
            .collection('data')
            .valueChanges()
            .subscribe(videos => {
                videos.forEach(video => {
                    this.videosCars.push(video);
                });
            });

    }


}
