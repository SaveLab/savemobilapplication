import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaveRealEstatePageRoutingModule } from './save-real-estate-routing.module';

import { SaveRealEstatePage } from './save-real-estate.page';
import {ComponentModule} from "../../shared/components/component.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SaveRealEstatePageRoutingModule,
        ComponentModule
    ],
  declarations: [SaveRealEstatePage]
})
export class SaveRealEstatePageModule {}
