import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SaveTechnologiesPage } from './save-technologies.page';

describe('SaveTechnologiesPage', () => {
  let component: SaveTechnologiesPage;
  let fixture: ComponentFixture<SaveTechnologiesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveTechnologiesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SaveTechnologiesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
