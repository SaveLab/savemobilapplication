import {Component, Input, OnInit} from '@angular/core';
import {ModalController, LoadingController, AlertController} from '@ionic/angular';
import {ModalService} from '../../shared/services/modal.service';
import {ToastService} from '../../shared/services/toast.service';
import {DataService} from '../../shared/services/data.service';
import {MediaService} from '../../shared/services/media.service';

import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import * as firebase from 'firebase';
import {AuthenticationService} from "../../shared/services/authentication-service.service";
import {MethodesService} from "../../shared/services/methodes.service";

@Component({
    selector: 'app-free-trial-car',
    templateUrl: './free-trial-car.page.html',
    styleUrls: ['./free-trial-car.page.scss'],
})
export class FreeTrialCarPage implements OnInit {

    briefInfo: string;
    optionMain: string;
    engineType: string;
    engineSize: string;
    fuelEfficiency: string;
    hoursePower: string;
    touque: string;
    driveWheel: string;
    transmission: string;
    carName: string;
    productionYear:number;
    price: number;
    phoneNumer: string;
    mileAge: string;
    accident: string;
    fuelType: string;
    location: string;
    interiorBack: string;
    interiorFront: string;
    left: string;
    trunk: string;
    right: string;
    bonet: string;
    registration: string;
    language = '';


    constructor(public modalController: ModalController,
                public modalService: ModalService,
                public dataService: DataService,
                public mediaService: MediaService,
                public loadingController: LoadingController,
                public alertController: AlertController,
                public afSG: AngularFireStorage,
                public authService: AuthenticationService,
                public afS: AngularFirestore,

    ) {
        if (this.authService.userData.freeCar){
            this.modalService.showFreeTrialAlreadyDoneModal('Save Cars');
        }
    }


    ngOnInit() {
    }

    close() {
        this.modalController.dismiss();
    }

  selectAveLanguage(event) {
    this.language = event.target.value;
  }


}
