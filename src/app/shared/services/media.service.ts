import {Injectable} from '@angular/core';
import {LoadingController, AlertController} from '@ionic/angular';
import {AuthenticationService} from './authentication-service.service';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import * as firebase from 'firebase';
import {ToastService} from "./toast.service";
import {MethodesService} from "./methodes.service";
import {ModalService} from "./modal.service";


@Injectable({
  providedIn: 'root'
})
export class MediaService {

  clips = [];



  constructor(
      public loadingController: LoadingController,
      public alertController: AlertController,
      public afSG: AngularFireStorage,
      public toastService: ToastService,
      public modalService: ModalService,
      public afS: AngularFirestore,
      public authService: AuthenticationService,
      public methodesService: MethodesService
  ) {
  }



  async selectFile(event: FileList, clip: string) {
    if (!this.clips) {
      this.clips = [{
        name: clip,
        file: event.item(0)
      }];
    } else {
      this.clips.push({
        name: clip,
        file: event.item(0)
      })
    }

  }


  async uploadFile(field, max: number, language: string, freeTrial: boolean, nameofField: string) {
    if (freeTrial){
      this.modalService.showFreeTrialAlreadyDoneModal(nameofField)
    }else{

      if (this.clips.length < max || !language) {
        this.toastService.fillAllInput();
      } else {
        let Files = {
          language: language
        };
        const loading = await this.loadingController.create({
          cssClass: 'my-custom-class',
          message: 'Upload video clip...',
          backdropDismiss: false,
          mode: 'ios'
        });


        await loading.present();
        const time = firebase.firestore.Timestamp.now().toMillis().toString();
        for (let i = 0; i <= max; i++) {
          const value = this.clips[i].name;
          const path = field + '/' + this.authService.userData.uid + '/ ' + time + '/ ' + value;
          const customMetadata = {app: 'clip video' + value};
          const fileRef = this.afSG.ref(path);
          this.afSG.upload(path, this.clips[i].file, {customMetadata}).then(() => {
            fileRef.getDownloadURL().subscribe(res => {
              Files[value] = res;
              this.afS.collection('Users')
                  .doc(this.authService.userData.uid)
                  .collection('renderedVideo')
                  .doc(field).collection('data')
                  .doc(time)
                  .set(Files);
              loading.dismiss();
            }, error => {
              loading.dismiss();
            });
          });
        }
        this.methodesService.goTo('home')

      }
    }



  }
}
