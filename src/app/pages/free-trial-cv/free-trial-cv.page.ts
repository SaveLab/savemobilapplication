import {Component, OnInit} from '@angular/core';
import {MethodesService} from "../../shared/services/methodes.service";
import {AlertController, LoadingController, ModalController} from "@ionic/angular";
import {ModalService} from "../../shared/services/modal.service";
import {DataService} from "../../shared/services/data.service";
import {MediaService} from "../../shared/services/media.service";
import {AngularFireStorage} from "@angular/fire/storage";
import {AuthenticationService} from "../../shared/services/authentication-service.service";
import {AngularFirestore} from "@angular/fire/firestore";

@Component({
    selector: 'app-free-trial-cv',
    templateUrl: './free-trial-cv.page.html',
    styleUrls: ['./free-trial-cv.page.scss'],
})
export class FreeTrialCVPage implements OnInit {

    profile: string;
    competency: string;
    name: string;
    birthYear: string;
    education: string;
    major: string;
    address: string;
    yearOfExperience: number;
    expectedWorkingArea: string;
    desiredPosition: string;
    skill: string;
    knowledge: string;
    strongPoint: string;
    phoneNumber: string;
    language = '';

    constructor(public modalController: ModalController,
                public modalService: ModalService,
                public dataService: DataService,
                public mediaService: MediaService,
                public loadingController: LoadingController,
                public alertController: AlertController,
                public afSG: AngularFireStorage,
                public authService: AuthenticationService,
                public afS: AngularFirestore,
    ) {
        if (this.authService.userData.freeCar) {
            this.modalService.showFreeTrialAlreadyDoneModal('Save Cvs');
        }
    }


    ngOnInit() {
    }

    close() {
        this.modalController.dismiss();
    }

    selectAveLanguage(event) {
        this.language = event.target.value;
    }

}
