import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SaveCVsPage } from './save-cvs.page';

const routes: Routes = [
  {
    path: '',
    component: SaveCVsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SaveCVsPageRoutingModule {}
