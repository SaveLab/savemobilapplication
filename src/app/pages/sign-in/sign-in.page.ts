import {Component, OnInit} from '@angular/core';

import {Router} from '@angular/router';
import {environment} from 'src/environments/environment';
import {AngularFirestore} from '@angular/fire/firestore';
import {AuthenticationService} from 'src/app/shared/services/authentication-service.service';
import {NavController} from '@ionic/angular';
import {LanguagesService} from "../../shared/services/languages.service";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit {

  username: any;
  password: any;
  connected: boolean;
  errorMessage: any;
  loading: boolean = false;


  constructor(
              private navCtrl: NavController,
              public router: Router,
              private authService: AuthenticationService,
              private languagesService: LanguagesService,
              public afStore: AngularFirestore) {}

  ngOnInit(): void {
  }

  login() {
    this.loading = true;
    this.authService.signIn(this.username, this.password).then(res => {
      this.afStore.collection('Users').doc(res.user.uid).get().subscribe(doc => {
        if ( doc.exists ) {
          const user = doc.data();
          console.log(user);
          this.languagesService.language = user.language;
          this.authService.setUserDataLocal(user);
          this.goTo('home');
        }
      });
      this.loading = false;
    }).catch(err => {
      this.loading = false;
      this.errorMessage = err.message;
      throw err
    });
  }

  goTo(page: string) {
    this.navCtrl.navigateRoot('/' + page);
  }
}
