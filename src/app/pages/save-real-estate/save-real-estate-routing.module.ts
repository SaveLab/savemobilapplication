import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SaveRealEstatePage } from './save-real-estate.page';

const routes: Routes = [
  {
    path: '',
    component: SaveRealEstatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SaveRealEstatePageRoutingModule {}
