import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaveCarPageRoutingModule } from './save-car-routing.module';

import { SaveCarPage } from './save-car.page';
import {ComponentModule} from "../../shared/components/component.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SaveCarPageRoutingModule,
        ComponentModule
    ],
  declarations: [SaveCarPage]
})
export class SaveCarPageModule {}
