import { Injectable } from '@angular/core';
import {AngularFireFunctions} from "@angular/fire/functions";

@Injectable({
  providedIn: 'root'
})
export class InAppPurchaseService {
  prices: any[];
  premiumPrice: any;

  constructor(public functions: AngularFireFunctions) { }

  createCustomer(user) {
    return new Promise((resolve, reject) => {
      const httpsCallable = this.functions.httpsCallable('createCustomer');
      httpsCallable({uid: user.uid, email: user.email}).subscribe(res => {
        resolve(res);
      }, error => {
        reject(error);
      });
    });
  }

  updateCustomer(customerId, defaultSource) {
    return new Promise((resolve, reject) => {
      const httpsCallable = this.functions.httpsCallable('updateCustomer');
      httpsCallable({customerId, defaultSource}).subscribe(res => {
        resolve(res);
      }, error => {
        reject(error);
      });
    });
  }

  getCustomerCards(customerId) {
    return new Promise((resolve, reject) => {
      const httpsCallable = this.functions.httpsCallable('getCustomerCards');
      httpsCallable({customerId}).subscribe(res => {
        resolve(res.data);
      }, error => {
        reject(error);
      });
    });
  }

  createCard(customerId, cardToken) {
    return new Promise((resolve, reject) => {
      const httpsCallable = this.functions.httpsCallable('createCard');
      httpsCallable({customerId, cardToken}).subscribe(res => {
        resolve(res);
      }, error => {
        reject(error);
      });
    });
  }

  createCharge(amount, customerId, currency) {
    return new Promise((resolve, reject) => {
      const httpsCallable = this.functions.httpsCallable('createCharge');
      httpsCallable({amount, customerId, currency}).subscribe(res => {
        resolve(res);
      }, error => {
        reject(error);
      });
    });
  }

  createSubscription(customerId, price) {
    return new Promise((resolve, reject) => {
      const httpsCallable = this.functions.httpsCallable('createSubscription');
      httpsCallable({customerId, price}).subscribe(res => {
        resolve(res);
      }, error => {
        reject(error);
      });
    });
  }

  getPrices(currency) {
    const httpsCallable = this.functions.httpsCallable('getPrices');
    httpsCallable({currency}).subscribe(res => {
      this.prices = res.data;
    });
  }

  getPremiumPrice(currency) {
    const httpsCallable = this.functions.httpsCallable('getPremiumPrice');
    httpsCallable({currency}).subscribe(res => {
      this.premiumPrice = res.data;
    });
  }

  findPrice(productId) {
    let res = 0;
    this.prices.forEach(price => {
      if ( price.product === productId) {
        res = price.unit_amount;
      }
    });
    return res;
  }

  registerProducts() {

  }

  subscribePremium() {

  }

  findProduct(id) {

  }
}
