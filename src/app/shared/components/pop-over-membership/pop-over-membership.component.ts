import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../services/authentication-service.service";
import {NavController} from "@ionic/angular";
import { PopoverController } from '@ionic/angular';
import {LanguagesService} from "../../services/languages.service";



@Component({
  selector: 'app-pop-over-membership',
  templateUrl: './pop-over-membership.component.html',
  styleUrls: ['./pop-over-membership.component.scss'],
})
export class PopOverMembershipComponent implements OnInit {

  constructor(
      public navCtrl: NavController,
      public authService: AuthenticationService,
      public languageService: LanguagesService,
      public popover: PopoverController
  ) { }

  ngOnInit() {}

  signOut() {
    this.close();
    this.authService.signOut().then(() => {
      this.authService.deleteUserDataLocal();
      this.goTo('home');

    });
  }


  goTo(page: string) {
    this.navCtrl.navigateRoot('/' + page);
    this.close()
  }
  close(){
    this.popover.dismiss();
  }




}
