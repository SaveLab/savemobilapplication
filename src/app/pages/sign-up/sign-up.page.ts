import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {AngularFirestore} from '@angular/fire/firestore';
import {AuthenticationService} from '../../shared/services/authentication-service.service';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.page.html',
    styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

    email: any;
    username: any;
    password: any;
    errorMessage: any;

    constructor(private navCtrl: NavController,
                private authService: AuthenticationService,
                public afStore: AngularFirestore) {
    }

    ngOnInit() {
    }

    signUp() {
        this.authService.registerUser(this.email, this.password).then(res => {
            const user = {
                username: this.username,
                uid: res.user.uid,
                email: res.user.email,
                language: 'en',
                token: '',
                accepted: false,
                freeCar: false,
                freeCv: false,
                freeRe: false,
                pricingPlan: 'none',
                videoRendered: 0
            };
            this.afStore.collection('Users').doc(res.user.uid).set(user);
            this.authService.setUserDataLocal(user);
          this.goTo('home');
        }).catch(err => {
            this.errorMessage = err.message;
            throw err;
        });
    }

    goTo(page: string) {
        this.navCtrl.navigateRoot('/' + page);
    }

}
