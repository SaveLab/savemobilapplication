import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SaveCarPage } from './save-car.page';

const routes: Routes = [
  {
    path: '',
    component: SaveCarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SaveCarPageRoutingModule {}
