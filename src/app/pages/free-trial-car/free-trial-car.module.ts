import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FreeTrialCarPageRoutingModule } from './free-trial-car-routing.module';

import { FreeTrialCarPage } from './free-trial-car.page';
import {ComponentModule} from "../../shared/components/component.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FreeTrialCarPageRoutingModule,
        ComponentModule
    ],
  declarations: [FreeTrialCarPage]
})
export class FreeTrialCarPageModule {}
