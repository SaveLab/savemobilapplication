import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from "@ionic/angular";

@Component({
  selector: 'app-free-trials-done',
  templateUrl: './free-trials-done.component.html',
  styleUrls: ['./free-trials-done.component.scss'],
})
export class FreeTrialsDoneComponent implements OnInit {

  @Input() title:string;

  constructor(public modalController: ModalController) { }

  close() {
    this.modalController.dismiss();
  }


  ngOnInit() {}

}
