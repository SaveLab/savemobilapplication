import { Injectable } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {UploadHelpComponent} from '../modals/upload-help/upload-help.component';
import {FreeTrialsDoneComponent} from "../modals/free-trials-done/free-trials-done.component";

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(public modalController: ModalController) {

  }

  async showUploadHelpModal(image: any, title: any) {
    const modal = await this.modalController.create({
      component: UploadHelpComponent,
      componentProps: {
        image,
        title
      }
    });
    await modal.present();

    return modal.onWillDismiss();
  }
  async showFreeTrialAlreadyDoneModal(title: any) {
    const modal = await this.modalController.create({
      component: FreeTrialsDoneComponent,
      componentProps: {
        title
      }
    });
    await modal.present();

    return modal.onWillDismiss();
  }
}
