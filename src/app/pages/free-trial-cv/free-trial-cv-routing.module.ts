import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FreeTrialCVPage } from './free-trial-cv.page';

const routes: Routes = [
  {
    path: '',
    component: FreeTrialCVPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FreeTrialCVPageRoutingModule {}
