// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  stripeKey: 'pk_test_51H4ioCE8ESXHasdjy3k2GYCVGCeiGmvWh1bT6ZPu5H04pEsOArE0jxE1CuMWBMVhFgSQha4xm7mjjw4RbRbkv3XK00kp5y2YSW',
  firebaseConfig: {
    apiKey: "AIzaSyDoytxbRXK9xb_y64LFHGzf_eHjX6WKQtY",
    authDomain: "save-technologies-5e963.firebaseapp.com",
    databaseURL: "https://save-technologies-5e963.firebaseio.com",
    projectId: "save-technologies-5e963",
    storageBucket: "save-technologies-5e963.appspot.com",
    messagingSenderId: "6744338013",
    appId: "1:6744338013:web:bf1d67cedc5c70ca63a7b4",
    measurementId: "G-PQJZEZ5BDQ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
