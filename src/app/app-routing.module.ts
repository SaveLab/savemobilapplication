import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {IsSignedInGuard} from "./shared/guards/is-signed-in.guard";
import {IsNotSignedInGuard} from "./shared/guards/is-not-signed-in.guard";

const routes: Routes = [
    {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
    },

    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },

    {
        path: 'save-car',
        loadChildren: () => import('./pages/save-car/save-car.module').then(m => m.SaveCarPageModule)
    },
    {
        path: 'save-real-estate',
        loadChildren: () => import('./pages/save-real-estate/save-real-estate.module').then(m => m.SaveRealEstatePageModule)
    },
    {
        path: 'save-cvs',
        loadChildren: () => import('./pages/save-cvs/save-cvs.module').then(m => m.SaveCVsPageModule)
    },
    {
        path: 'sign-up',
        loadChildren: () => import('./pages/sign-up/sign-up.module').then(m => m.SignUpPageModule),
        canActivate: [
            IsNotSignedInGuard
        ]
    },
    {
        path: 'sign-in',
        loadChildren: () => import('./pages/sign-in/sign-in.module').then(m => m.SignInPageModule),
        canActivate: [
            IsNotSignedInGuard
        ]
    },
    {
        path: 'forgot-password',
        loadChildren: () => import('./pages/forgot-password/forgot-password.module').then(m => m.ForgotPasswordPageModule)
    },
    {
        path: 'my-account',
        loadChildren: () => import('./pages/my-account/my-account.module').then(m => m.MyAccountPageModule),
        canActivate: [
            IsSignedInGuard
        ]
    },
    {
        path: 'free-trial-cv',
        loadChildren: () => import('./pages/free-trial-cv/free-trial-cv.module').then(m => m.FreeTrialCVPageModule),

    },
    {
        path: 'free-trial-car',
        loadChildren: () => import('./pages/free-trial-car/free-trial-car.module').then(m => m.FreeTrialCarPageModule)
    },
    {
        path: 'free-trial-re',
        loadChildren: () => import('./pages/free-trial-re/free-trial-re.module').then(m => m.FreeTrialREPageModule)
    },
  {
    path: 'save-technologies',
    loadChildren: () => import('./pages/save-technologies/save-technologies.module').then( m => m.SaveTechnologiesPageModule)
  },

];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
