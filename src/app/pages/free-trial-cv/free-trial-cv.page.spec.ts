import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FreeTrialCVPage } from './free-trial-cv.page';

describe('FreeTrialCVPage', () => {
  let component: FreeTrialCVPage;
  let fixture: ComponentFixture<FreeTrialCVPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeTrialCVPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FreeTrialCVPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
