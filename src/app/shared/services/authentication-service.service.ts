import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  userData: any = null;
  userReference: any;
  userExtraReference: any;
  userExtraData: any;
  constructor(public ngFireAuth: AngularFireAuth,
              public aFS: AngularFirestore,
              public router: Router) {


  }

  // Login in with email/password
  signIn(email, password) {
    return this.ngFireAuth.signInWithEmailAndPassword(email, password);
  }

  // Register user with email/password
  registerUser(email, password) {
    return this.ngFireAuth.createUserWithEmailAndPassword(email, password);
  }

  // Recover password
  passwordRecover(passwordResetEmail) {
    return this.ngFireAuth.sendPasswordResetEmail(passwordResetEmail);
  }

  get isLoggedIn(): boolean {
    return this.userData !== null;
  }
  get checkSubscribe(){
    return this.isLoggedIn && this.userData.pricingPlan !== 'none';
  }

  setUserDataLocal(user) {

    this.userData = user;
    this.userReference = this.aFS.collection('Users').doc(user.uid);
    this.userExtraReference = this.aFS.collection('UsersExtra').doc(user.uid);
    localStorage.setItem('user', JSON.stringify(user));
  }

  deleteUserDataLocal() {
    localStorage.removeItem('user');
    this.userData = null;
    this.userReference = null;
    this.userExtraData = null;
  }


  updateUserDataFirebase() {
    return this.userReference.set(this.userData, {
      merge: true
    });
  }

  updateUserExtraDataFirebase() {
    return this.userExtraReference.set(this.userExtraData, {
      merge: true
    });
  }

  signOut() {
    return this.ngFireAuth.signOut();
  }


  getCurrentUser() {
    return this.ngFireAuth.currentUser;
  }

  getCurrentCurrencySymbol() {
    switch (this.userExtraData.currency) {
      case 'eur':
        return '€';
      default:
        return '$';
    }
  }
}
