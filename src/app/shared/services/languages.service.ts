import {Injectable, OnInit} from '@angular/core';
import {AuthenticationService} from "./authentication-service.service";


@Injectable({
    providedIn: 'root'
})
export class LanguagesService implements OnInit {
    language: any;

    constructor(public authService: AuthenticationService) {

        if(this.authService.isLoggedIn){
            this.language = this.authService.userData.language;
        }else{
            this.language = 'en';
        }
        console.log('la langue est: ' + this.language);
    }

    ngOnInit(): void {
    }


    selectLanguage(event) {

        this.language = event.target.value;
        if (this.authService.isLoggedIn) {
            this.authService.userData.language = this.language;
            this.authService.updateUserDataFirebase();
        }

    }

    mode() {
        return this.languages[this.language];
    }


    languages = {
        en: {
            lang: 'Language',
            signIn: 'Sign In',
            signUp: 'Sign Up',
            signOut: 'Log Out',
            myAccount: 'My account',
            whoWeAre: 'Who We Are ?',
            korean: 'Korean',
            english: 'English',
            french: 'French',
            home: 'Home',
            ourProducts: 'Our Products',
            aboutUs: 'About Us',
          moreInformation: 'More Information',
          freeTrial: 'Free Trial'
        },
        fr: {
            lang: 'Langue',
            signIn: 'Se connecter',
            signUp: 'S inscrire',
            signOut: 'Se deconnecter',
            myAccount: 'Mon compte',
            whoWeAre: 'Qui sommes-nous ?',
            korean: 'Coréen',
            english: 'Anglais',
            french: 'Francais',
            home: 'Acceuil',
            ourProducts: 'Nos Produits',
            aboutUs: 'A Propos',
          moreInformation: 'Plus d information',
          freeTrial: 'Essai gratuit'

        },
        kr: {
            signIn: 'Sign In',
            signUp: 'Sign Up'
        }
    }
}
