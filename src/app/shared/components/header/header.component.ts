import { Component, OnInit } from '@angular/core';
import {PopoverController} from "@ionic/angular";
import {AuthenticationService} from "../../services/authentication-service.service";
import {PopOverMembershipComponent} from "../pop-over-membership/pop-over-membership.component";
import {MethodesService} from "../../services/methodes.service";
import {LanguagesService} from "../../services/languages.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {


  constructor(
      public popoverController: PopoverController,
      private authService: AuthenticationService,
      public languageService: LanguagesService,
      public methodesService: MethodesService
  ) {
  }

  ngOnInit() {
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopOverMembershipComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

}
