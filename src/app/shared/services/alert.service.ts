import {Injectable} from '@angular/core';
import {AlertController, ToastController} from "@ionic/angular";
import {ToastService} from "./toast.service";
import {DataService} from "./data.service";

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor(
        public dataService: DataService,
        public alertController: AlertController,
        public toastService: ToastService,
    ) {
    }


    async alertDeleteVideo(name: string, field: string, videoId: string) {
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'This will delete the video ' + name + ' from your account',
            message: 'Are you sure you want to complete' ,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.dataService.deleteVideoRendered(field, videoId);
                        this.toastService.videoDeleted()
                    }
                }
            ]
        });

        await alert.present();
    }

}
