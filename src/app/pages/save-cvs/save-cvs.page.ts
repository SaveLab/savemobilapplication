import {Component, OnInit} from '@angular/core';
import {MethodesService} from "../../shared/services/methodes.service";
import {ModalService} from "../../shared/services/modal.service";
import {AuthenticationService} from "../../shared/services/authentication-service.service";

@Component({
    selector: 'app-save-cvs',
    templateUrl: './save-cvs.page.html',
    styleUrls: ['./save-cvs.page.scss'],
})
export class SaveCVsPage implements OnInit {

    selectedValues: string;

    constructor(
        public methodesService: MethodesService,
        public modalService: ModalService,
        public authService: AuthenticationService
    ) {
        this.selectedValues = 'howitworks'


    }

    ngOnInit() {
    }

    segmentChanged(ev: any) {
        this.selectedValues = ev.target.value;

    }

    goToFreeTrial(){
        if(this.authService.userData.freeCv){
            this.seePricing();
            this.modalService.showFreeTrialAlreadyDoneModal('Save Cv');
        }else {
            this.methodesService.goToFreeTrial('free-trial-cv');
        }
    }

    seePricing() {
        this.selectedValues = 'pricing';
    }

}
