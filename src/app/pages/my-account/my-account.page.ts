import { Component, OnInit } from '@angular/core';
import {DataService} from '../../shared/services/data.service'
import {AlertService} from "../../shared/services/alert.service";
import {ModalController, ToastController} from "@ionic/angular";
import {AuthenticationService} from "../../shared/services/authentication-service.service";
import {ModalCreditCardComponent} from "../../shared/modals/modal-credit-card/modal-credit-card.component";
import {InAppPurchaseService} from "../../shared/services/in-app-purchase.service";

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.page.html',
  styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage implements OnInit {

  constructor(public dataService: DataService,
              public alertService: AlertService,
              public toastController: ToastController,
              public authService: AuthenticationService,
              public inAppPurchaseService: InAppPurchaseService,
              public modalController: ModalController) { }

  ngOnInit() {
  }




}
