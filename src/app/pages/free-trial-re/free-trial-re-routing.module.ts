import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FreeTrialREPage } from './free-trial-re.page';

const routes: Routes = [
  {
    path: '',
    component: FreeTrialREPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FreeTrialREPageRoutingModule {}
