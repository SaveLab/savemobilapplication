import { Component, OnInit } from '@angular/core';
import {MethodesService} from "../../shared/services/methodes.service";
import {ModalService} from "../../shared/services/modal.service";
import {AuthenticationService} from "../../shared/services/authentication-service.service";

@Component({
  selector: 'app-save-real-estate',
  templateUrl: './save-real-estate.page.html',
  styleUrls: ['./save-real-estate.page.scss'],
})
export class SaveRealEstatePage implements OnInit {

  selectedValues: string;

  constructor(
      public methodesService: MethodesService,
      public modalService: ModalService,
      public authService: AuthenticationService
  ) {
    this.selectedValues = 'howitworks'


  }

  ngOnInit() {
  }

  segmentChanged(ev: any) {
    this.selectedValues = ev.target.value;

  }

  goToFreeTrial(){
    if(this.authService.userData.freeRe){
      this.seePricing();
      this.modalService.showFreeTrialAlreadyDoneModal('Save RealEstate');
    }else {
      this.methodesService.goToFreeTrial('free-trial-re');
    }
  }

  seePricing() {
    this.selectedValues = 'pricing';
  }

}
