import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FreeTrialCVPageRoutingModule } from './free-trial-cv-routing.module';

import { FreeTrialCVPage } from './free-trial-cv.page';
import {ComponentModule} from "../../shared/components/component.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FreeTrialCVPageRoutingModule,
        ComponentModule
    ],
  declarations: [FreeTrialCVPage]
})
export class FreeTrialCVPageModule {}
